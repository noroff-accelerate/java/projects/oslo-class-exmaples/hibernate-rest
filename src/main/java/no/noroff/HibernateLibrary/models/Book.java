package no.noroff.HibernateLibrary.models;

import com.fasterxml.jackson.annotation.*;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
//@JsonIdentityInfo(
//        generator = ObjectIdGenerators.PropertyGenerator.class,
//        property = "id")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;


    @NaturalId
    @Column(name = "isbn_10")
    private String isbn10;
    @NaturalId
    @Column(name = "isbn_13")
    private String isbn13;

    private String edition;
    @Column(name = "publish_date")
    private Date publishDate;

    @ManyToOne
    @JoinColumn(name = "author_id")
    public Author author;

    @JsonGetter("author")
    public String author() {
        if(author != null){
            return "/api/v1/authors/" + author.getId();
        }else{
            return null;
        }
    }
    // Book entity
    @ManyToMany(mappedBy = "books")
    public List<Library> libraries;

    @JsonGetter("libraries")
    public List<String> librariesGetter() {
        if(libraries != null){
            return libraries.stream()
                    .map(library -> {
                        return "/api/v1/libraries/" + library.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }


    // Constructors

    // Getters and Setters


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsbn10() {
        return isbn10;
    }

    public void setIsbn10(String isbn10) {
        this.isbn10 = isbn10;
    }

    public String getIsbn13() {
        return isbn13;
    }

    public void setIsbn13(String isbn13) {
        this.isbn13 = isbn13;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Library> getLibraries() {
        return libraries;
    }

    public void setLibraries(List<Library> libraries) {
        this.libraries = libraries;
    }
}
